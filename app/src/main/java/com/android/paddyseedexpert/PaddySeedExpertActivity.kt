package com.android.paddyseedexpert

import android.Manifest
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import com.android.paddyseedexpert.fragments.SplashScreenFragment
import com.android.paddyseedexpert.permission.AbstractRuntimePermission

class PaddySeedExpertActivity : AbstractRuntimePermission() {

    private val REQUEST_CODE: Int = 13
    private var doubleBackToExitPressedFlag = false

    override fun onPermissionsGranted(requestCode: Int) {
        //Toast.makeText(applicationContext, "Permission granted $requestCode\n\n$REQUEST_CODE", Toast.LENGTH_LONG).show()
        Log.i("onPermissionsGranted", "requestCode: $requestCode")
        if (requestCode == REQUEST_CODE) {
            var fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.activityFrame, SplashScreenFragment(), "Splash screen fragment")
            //fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        requestAppPermissions(
            arrayOf(
                Manifest.permission.INTERNET,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ), R.string.permission_error_message, REQUEST_CODE)
    }

    override fun onBackPressed() {

        if(isUserOnHomeScreen()){
            //Log.i("onBackPressed","User on HomeScreen")
            if(doubleBackToExitPressedFlag){
                activityCloseOperations()
                super.onBackPressed()
                return
            }
            doubleBackToExitPressedFlag = true
            Toast.makeText(this, "Click back again to exit", Toast.LENGTH_SHORT).show()
            Handler().postDelayed({
                doubleBackToExitPressedFlag = false
            }, 2000)
            //super.onBackPressed()
        } else {
            activityCloseOperations()
            super.onBackPressed()
        }

    }

    private fun activityCloseOperations() {


    }

    private fun isUserOnHomeScreen(): Boolean {
        val intentCategories = intent.categories
        return intentCategories!=null && intentCategories.contains("android.intent.category.LAUNCHER")
    }

}
