package com.android.paddyseedexpert.fragments

import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.paddyseedexpert.R

class SplashScreenFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_splash_screen, container, false)
    }


    override fun onStart() {
        super.onStart()

        Handler().postDelayed({
            startAuthentication()
        }, 5000) //show splash screen for 5 seconds and then show authentication screen

    }

    private fun startAuthentication() {
        var fragmentTransaction = activity?.supportFragmentManager?.beginTransaction()!!
        fragmentTransaction.replace(R.id.activityFrame, AuthenticationFragment(), "Authentication fragment")
        //fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

}
