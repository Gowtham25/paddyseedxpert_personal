package com.android.paddyseedexpert.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

import com.android.paddyseedexpert.R
import kotlinx.android.synthetic.main.fragment_authentication.*

/**
 * A simple [Fragment] subclass.
 */
class AuthenticationFragment : Fragment(), View.OnClickListener {

    override fun onClick(view: View?) {

        when(view) {
            login -> Toast.makeText(activity?.applicationContext, "User login invoked", Toast.LENGTH_SHORT).show()
            forgotPassword -> Toast.makeText(activity?.applicationContext, "Forgot password invoked", Toast.LENGTH_SHORT).show()
            else -> Toast.makeText(activity?.applicationContext, "Invalid click action", Toast.LENGTH_SHORT).show()
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var inflatedView = inflater.inflate(R.layout.fragment_authentication, container, false)

        inflatedView.findViewById<Button>(R.id.login).setOnClickListener(this)
        inflatedView.findViewById<TextView>(R.id.forgotPassword).setOnClickListener(this)

        return inflatedView
    }


}
