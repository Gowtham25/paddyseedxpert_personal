
if [ -z $CIRCLE_TAG ];
  then
    echo "CIRCLE_TAG property is unset. Not a tagged build. Skipping deployment...";
#    echo "Starting to list files under build_backup directory";
#    ls -lahR /home/circleci/build_backup
#    echo "File listing complete.....";
    exit 0;
else
  echo "Tagged build detected. CIRCLE_TAG is set to '$CIRCLE_TAG'";
  echo "Prepping to upload artifact to bitbucket...";
  VERSION=$(echo $CIRCLE_TAG | cut -d'-' -f2) #Cuts the tag into two parts based on the de-limiter '-' and gives second half
  UNSIGNED_APK_BASE_LOCATION="../build_backup/outputs/apk/release";
  UNSIGNED_APK_BASE_NAME="app-release.apk";
  UNSIGNED_APK_NEW_NAME="PaddySeedExpert-$VERSION.apk";
  UNSIGNED_APK_OLD_LOCATION="$UNSIGNED_APK_BASE_LOCATION/$UNSIGNED_APK_BASE_NAME";
  UNSIGNED_APK_NEW_LOCATION="$UNSIGNED_APK_BASE_LOCATION/$UNSIGNED_APK_NEW_NAME";
  # echo -e "\nVERSION: $VERSION\nUNSIGNED_APK_BASE_LOCATION: $UNSIGNED_APK_BASE_LOCATION\nUNSIGNED_APK_BASE_NAME: $UNSIGNED_APK_BASE_NAME\nUNSIGNED_APK_NEW_NAME: $UNSIGNED_APK_NEW_NAME\nUNSIGNED_APK_OLD_LOCATION: $UNSIGNED_APK_OLD_LOCATION\nUNSIGNED_APK_NEW_LOCATION: $UNSIGNED_APK_NEW_LOCATION\n";
  mv $UNSIGNED_APK_OLD_LOCATION $UNSIGNED_APK_NEW_LOCATION;
  # echo "Move successful...";
  # echo -e "TEST_PIPELINE_BB_AUTH_STRING:- $TEST_PIPELINE_BB_AUTH_STRING\nBITBUCKET_REPO_OWNER:- $BITBUCKET_REPO_OWNER\nBITBUCKET_REPO_SLUG:- $BITBUCKET_REPO_SLUG";
  curl -v -X POST "https://${TEST_PIPELINE_BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"$UNSIGNED_APK_NEW_LOCATION"
fi

